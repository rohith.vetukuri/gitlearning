console.log("Basic Programs");
//Math Functions

let minimumvalue = Math.min(3, 5, 1);
console.log(minimumvalue)

let maximumvalue = Math.max(10, 3, 89);
console.log(maximumvalue)

console.log(Math.ceil(10.47))

console.log(Math.floor(12.89))

let randomnumber = Math.random();
//Random Number 1 digit 
//Random Number 2 digit
//Random Number 3 digit
console.log(randomnumber)

console.log(Math.pow(2, 10))


//Functions 
//Function Declaration
function isEven(num) {
    if (num % 2 == 0) {

        return "Even Number"
    }
    else {
        return "Odd Number"
    }

}

//Function calling
let result = isEven(2);
console.log(result)


//WAP  to find the Area of a rectangle if Length and breadth 
//are greater than one

//Input : 2,3
//Output: The area of a rectangle is 6

//Input :2,-3
//Output :Not a valid value,


function areaOfRectangle(length, breadth) {
    if (length > 0 && breadth > 0) {
        let result = "The area of rectangle is " + (length * breadth)
        return result

    } else {
        return "Not a valid number"

    }

}


let result1 = areaOfRectangle(2, -3)
console.log(result1)


//Write a program to check whether given year is leap year or not 

//Input : 1998 
//Output :false

//Input:2016
//Output:true

//Input : 2000
//Output:


function leapYear(year) {
    if (year % 4 === 0) {
        if (year % 100 === 0) {
            if (year % 400 === 0) {
                return true

            } else {
                return false
            }

        } else {
            return false
        }


    } else {
        return false
    }

}

console.log(leapYear(2014))

//write a program to find the number of days in a given year

//Input  : 2024,2
//Output : 29 days

//Input : 2016,5
//Output : 31

// 1,3,5,7,8,10,12 -- return 31 days
// 4,6,9,11  -- return 30 days
// 2   -- it is a leap year or not  ,if leap 29 days else 28days 



//WAP to find the largest number among 3 numbers

//Input : 12,4,9
//Output : 12

function largestNumber(a , b , c){
    if(a >b && a >c){
        return ` ${a} is largest number `
       
    }else if(b>c){
        return `${b} is largest number`

    }else{
        return `${c} is a largest number`
    }


}

// let num1 = prompt("Enter first number");
// let num2 = prompt("Enter second number");
// let num3 = prompt("Enter third  number")
// console.log(`${num1} is number1 ${num2} is a number2 ${num3} is a number3 `)

// let largest_number = largestNumber(num1,num2 , num3)
// console.log(largest_number)



//Looping statements
//For Loop 

// for(initialization ;condition ; incre /decre){
//     //statements
// }

//WAP to print hello 10 times

for(let count = 1 ; count <= 10 ; count++){
    console.log("Hello"+" " + count)
}


//WAP to print 1 to 10 

//WAP to print the table with out the table

//2 * 1 = 2
//2 * 2 = 4
function table(num){
    let complete_table = ""

    for(let i = 1 ;i <= 10 ; i++){
         complete_table +=(`${num} * ${i} = ${num * i}\n`)
    }

    return complete_table;


}


console.log(table(2))


username  = "gayathri"
// console.log(name[0])
// console.log(name[1])
// console.log(name[2])
// console.log(name[1])

for(let i = 0 ; i < username.length ; i++){
    console.log(username[i])
}
























