//String length
let str="Hello, World!"
console.log(str.length)

//String accessing
console.log(str[2])
console.log(str[7])

//String concatnation
let str1="Hello,"
let str2="JavaScript"
let str3=str1.concat(str2)
console.log(str3)
let stri3=str1+str2
console.log(str3)

//Substrings
console.log(str.substring(0,5))
console.log(str.substr(0,5))
console.log(str.substring(0,-1))

//Substring extraction
console.log(str.slice(1,6))
console.log(str.slice(0,-1))

let strr="Hello, JavaScript"
console.log(strr.indexOf("Javascript"))
//console.log(strr.lastIndexOf())

//String Conversion
let num=42;
let str11=num.toString();
console.log(typeof str11)

//String Upper and lowercase
let stri="Hello,World"
console.log(stri.toUpperCase())
console.log(stri.toLowerCase())

//searching in a string
console.log(stri.indexOf("World"))

//Replacing substrings
let strin="Hello,World"
let newstr=strin.replace("World","JavaScript")
console.log(newstr)
console.log(strin.charAt(2))

let Asciicode="Hello JavaScript"
console.log(Asciicode.charCodeAt("v"))

