//1) Number is positive or not
function posneg(num) {
    if (num > 0) {
        return "Positive Number";
    }
    else if (num < 0) {
        return "Negative Number";
    }
    else {
        return "Zero";
    }
}
console.log(posneg(6))
console.log(posneg(-3))
console.log(posneg(0))

//2) Number is Even or Odd
function evenodd(num) {
    if (num % 2 == 0)
        return "Even Number";

    else
        return "Odd Number";
}
console.log(evenodd(4))

//3)Greatest of Two Numbers

function GreatestOfTwo(num1, num2) {
    if (num1 > num2)
        return "num1 is Greater";
    else
        return "num2 is Greater";
}
console.log(GreatestOfTwo(3, 5))

//4)Grade Calculator
function GradeCalculator(score) {

    if (score >= 90)
        return "A";
    else if (score >= 80)
        return "B";
    else if (score >= 70)
        return "C";
    else if (score >= 60)
        return "D";
    else
        return "F"
}
console.log(GradeCalculator(50))
console.log(GradeCalculator(90))
console.log(GradeCalculator(70))

//5)Ticket Pricing
function TicketPricing(age) {
    if (age < 12)
        return "5";
    else if (age >= 12 && age <= 18)
        return "10";
    else if (age >= 18 && age <= 60)
        return "20";
    else
        return "15";
}
console.log(TicketPricing(25))

//6) Leap Year or not
function leapyear(year) {
    if (year % 4 == 0) {

        if (year % 100 != 0 || year % 400 == 0) {
            return "Leap year";
        }
    }
    else {
        return "Not a Leap Year";
    }
}
console.log(leapyear(2024))
console.log(leapyear(2011))

//7)Shipping Discount
function ShippingDiscount(purchaseAmount) {
    if (purchaseAmount >= 100)
        return "20"
    else if (purchaseAmount >= 50)
        return "10"
    else
        return "0"
}
console.log(ShippingDiscount(120))

//8)Time of Day Greeting

var currentTime=new Date();
    var currentHour=currentTime.getHours();
  
    var greeting;
   if(currentTime <12)
   greeting="Good Morning!"
   else if(currentTime <18)
   greeting="Good Afternoon!"
   else
   greeting="Good Evening!"
console.log(greeting);

//9)BMI Calculator
function BMICalculator(height,weight)
{ var bmi;
  var bmi=weight /(height * weight)
  var category;
  if(bmi< 18.5)
  return category="UnderWeight";
 else if(bmi< 24.9)
  return category="Normal Weight";
else if(bmi <29.9)
 return category ="OverWeight";
else
 return category="Obese";
}
console.log(BMICalculator(1.78,80))

//10)Number Guessing Game
function NumberGuess(secretnum,guessnum)
{
if(guessnum==secretnum)
return "Congratulations! You Have Guesssed the correct answer";
else if(guessnum < secretnum)
return "Try a higher Number!";
else
return "Try a lower Number!";
}
console.log(NumberGuess(7,5))


