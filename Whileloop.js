//initailization
//while(condition)
//{
    //statements
    //updation
//}

// Program to print 1 to 10 using while loop
let i=1;
while(i <= 10)
{
    console.log(i)
    i++
}

//sum of n natural numbers
function SumofNatural(n)
{
let count= 1
let sum= 0
while(count <= n)
{
    sum=sum + count
    count++
}
return sum
}
console.log(SumofNatural(10))

//String is palindrome or not

function isPalindrome(name)
{
   
    let reversedname=""

    let i=name.length - 1
    while(i>=0)
    {
        reversedname += name[i]
        i--
    }
    return reversedname == name
}
console.log(isPalindrome("VRV"))

//Reverse of a number

function ReverseNumber(num)
{
    let reversednum=""

    let i=num.length-1
      
    while(i>=0)
    {
        reversednum += num[i]
        i--
    }
    return reversednum
}
console.log(ReverseNumber('123'))

//factorial of number
function factorial(num)
{
    let i=1
    let fact=1
   if(i> num){
    return "Undefined"
   }
   while(i<=num)
   {
    fact=fact*i
    i++

   }
   return fact

}
console.log(factorial(5))

//Power of a number
function PowerNumber(n1,n2)
{ 
    let power=n1**n2;
    return power;
}
console.log(PowerNumber(3,4))

function Revnumber(num)
{
    let revnum=""
    for(let i=num.length-1;i>=0;i--)
    {
        revnum+=num[i]
    }
    return revnum
}
console.log(Revnumber("123"))

//Reverse a number using Mathfunctions
function reverse(num){
let Reversed=0
while(num!=0){
    Reversed=(Reversed * 10)+ num % 10
    num=Math.floor(num/10)
}
return Reversed
}
console.log(reverse(1234))


function Prime(num)
{
    if(num<=1)
    return "false"
 for(let i=2;i<=num;i++)
 {
    if(num%i==0)
    {
        return "false"
    }
    return "true"
 }
 return num
}
console.log(Prime(6))


function SumofEvenDigits(num)
{
    let sum = 0
while (num > 0) {
  r = num % 10;
  if (r % 2 == 0) {
    sum = sum + r;
  }
  num = parseInt(num / 10);
}
return sum
}
console.log(SumofEvenDigits(1234))

function SumOfEven(numb){
    let summ=0
    while(numb>0)
    {
        r=numb % 10;
        if(r % 2==0)
        {
            summ +=r;
        }
        numb=parseInt(numb/10)
    }
    return summ
}
console.log(SumOfEven(1234))

//break and continue using while and for loop
let count=1

while(count <= 10)
{
    if(count==5)
    break
console.log(count)
count++
}

let countt=1

while(countt <= 10)
{
    if(countt==5){
    countt++
    continue
    }
console.log(countt)
countt++
}

for(let i=1; i<=10; i++)
{
    if(i==8)
    {
        break
    }
    console.log(i)
}
for(let i=1; i<=10; i++)
{
    if(i==8)
    {
        continue
    }
    console.log(i)
}

//switch statements

let grade="A"

switch(grade)
{
    case "A":
        console.log("Excellent")
        break
    case "B":
        console.log("Good")
        break
    case "C":
        console.log("Average")
        break
    case "D":
        console.log("Below Average")
        break
    default:
        console.log("Fail")

}

//do while loop 

let a=1

do{
    console.log(a)
    a++
}while(a<=1)






//Quizz
let j=10;
do
{
    let num=prompt("Enter your number")

    if(num==j)
    {
        console.log(alert("You have guessed it right"));
        break;
    }
    else
    {
           console.log(alert("You answered it Wrong"));
           break;
    }
    j++
} while(j<=3)

