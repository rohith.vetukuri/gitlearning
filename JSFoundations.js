console.log("JS Foundations")

// Variables


// Declaration ,Intialization

// there are 3 key words for declaring
//var,let,const

//var --  global,redeclared ,reassign

// let   ---- block scoped ,cannot be redeclared  , can reassign

//const --    block scoped ,cannot be redeclared , cannot reassign

 
 let name = "rohith";

 name = 7678;

 console.log(name)


 const batch = 65;

//  batch = 67;


//Data types 

// Primitive Data types:  number, string ,boolean , undefined , null 

let a = 30;
console.log(typeof(a));

let username  = "Sai"
console.log(typeof(username))

let qualified = true;
console.log(typeof(qualified))

let age ;
console.log(typeof(age));


let batchnum = null;
console.log(typeof(batchnum))


// Array , Objects , functions

let skills  = [34,"html","css","bootstrap"]
console.log(typeof(skills))

console.log(skills[0])
console.log(skills.length)

skills[0] = "javascript";
console.log(skills)

skills[1]  = "Html5"
console.log(skills)

let admin  = "aman"
console.log(admin.length)


let person = {
    id:"1987848" , 
    name:"varma" , 
    age:18 ,
    'skills gained' : ["python","C++","Java"],
    learning : function(){
        console.log("learning fsd")
    }

}
console.log(person['skills gained'])




person.id = "hello"
console.log(person.id)


//Operators 
let num1 = 98;
let num2 = 786;
//Arithematic operators
console.log(num1 + num2)
console.log(num1 - num2)
console.log(num1 % num2)
console.log(num1 / num2)
console.log(num1 * num2)

// Assignment Operators

//   = , +=, -= , *= 

let num3 = 88;
console.log(num3 + 1)

// num3 = num3+1
num3 += 1
console.log(num3)

//num3 = num3 - 2
num3 -= 2
console.log(num3)


//Comparision operators



console.log(num1 == num2)
let d = 39;
let e = 39;
console.log(d === e)

console.log(d == e)

//Logical Operators

//  && || !

// True and True  --True
// False and False -- False
// False and True -- False
// True and False -- False

//Or
// True or True  --True
// False or False -- False
// False or True -- True
// True or False -- True

console.log( d == e || d < e)
console.log(!(d<=e) && d==e)

console.log(!true)


let age1 = 19
let result = age1 >18 ? "Major" : "Minor"
console.log(result)

let marks  =  75 ;

let passorfail = marks == 75 ? "Pass" : "Fail"

console.log(passorfail)




//Functions

//Why we need function
// 1.Code Reusability
// 2.Write once
//3.set of statements

//How to write functions

//Syntax
//Function declaration
// function function_name(){
//     //statements
// }


//Function calling 

// function_name()


// Example1 :  

function greet(){
    console.log("Hello! Good Morning 65 batch");
}

greet();
greet();

//write a function to print Hello! All welcome 

function greeting(name){
    console.log("Hello! " + name)
}

greeting("rohit")
greeting("rohith")
greeting("varma")

//write a program to print the square of a given number
function Square(num){
    console.log(num * num)

}


Square(5)
Square(3)
Square(15)

//Write a program to find the cube of a number

//WAP find the volume of cube with units   
 //Output : "The volume of cube is " + volume


//WAP to find the given number is even or odd
































 













 





//Datatypes -- string , boolean ,number ,undefined , null 
//Operators 












