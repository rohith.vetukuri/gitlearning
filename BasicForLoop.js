//1)Print 1 to 5
    for(let i=1;i<=5;i++)
    {
        console.log(i);
    }

//2) Print Even num 2 to 10

    for(let i=2;i<=10;i+=2)
    {
        console.log(i);
    }


//3)Sum of num 1 to 5

    let sum=0;
    for(let i=1; i<=5; i++)
    {
        sum+=i;
    }
    console.log(sum);

//4)Iterate Array and print elements
let array=[1,2,3,4,5]
    for( let i=0; i<array.length; i++)
    {
        console.log(array[i]);
    }


//5)loop through key-value pairs
let obj={a:1 ,b:2, c:3}
    for(let Key in obj)
    {
        console.log(obj);
    }

//6)Reverse Loop 5 to 1

    for(let i=5; i>=1;i--)
    {
        console.log(i);
    }


//7)Print Multiplication Table
let num =7;
    for(let i=1; i<=10; i++)
    {
        let result=num*i
    console.log(result);
    }


//8)Loop Array using forEach method()
let arra=[1, 2, 3, 4, 5]
    arra.forEach(element => {
        console.log(element);
    });


//9)Nested loop for Multiplication Table

    for(let i=1; i<=5; i++)
    {
        for(let j=1; j<=5; j++)
        {
    console.log(i*j);
    }
}


//10)Use Break or exit for loop

    for( let i=1; i<=10; i++)
    {
        if(i==5)
        {
            break;
        }
        console.log(i);
    }
